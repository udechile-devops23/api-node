.DEFAULT_GOAL := run
.PHONY: build, run_docker, run

run:
	@node app.js

build:
	@docker build -t api-node:1.0.0 .

run_docker: build
	@docker run -p 3000:3000 api-node:1.0.0