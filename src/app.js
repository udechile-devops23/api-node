
const express = require('express');
const os = require('os');
const logger = require('./logger');
const axios = require('axios');

const app = express();
const port = process.env.PORT || 3000;

app.use((req, res, next) => {
  const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
  const logData = {
    method: req.method,
    url: req.originalUrl,
    headers: req.headers,
    ip: ip,
  };
  logger.info('Request received', logData);
  next();
});

app.get('/', (req, res) => {
  res.send('Welcome to the API Node!');
});

app.get('/hello', (req, res) => {
  res.send('Hello World!');
});

app.get('/date', (req, res) => {
  res.send(new Date().toString());
});

app.get('/info', (req, res) => {
  const info = {
    hostname: os.hostname(),
    platform: os.platform(),
    arch: os.arch(),
    userInfo: os.userInfo(),
    cpuCores: os.cpus()
  };

  res.setHeader('Content-Type', 'application/json');
  res.send(JSON.stringify(info, null, 2));
});



const startServer = () => {
  const server = app.listen(port, () => {
    logger.debug(`API listening at http://localhost:${port}`);
  });
  return server;
};

if (require.main === module) {
  startServer();
}

module.exports = { app, startServer };
