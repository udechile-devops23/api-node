const request = require('supertest');
const { app, startServer } = require('../../app');
const logger = require('../../logger');

const server = startServer();

afterAll(() => {
  server.close();
});

describe('GET /', () => {
  it('responds with "Welcome to the API Node!"', async () => {
    const response = await request(app).get('/');
    expect(response.statusCode).toBe(200);
    expect(response.text).toBe('Welcome to the API Node!');
  });
});

describe('GET /hello', () => {
  it('responds with "Hello World!"', async () => {
    const response = await request(app).get('/hello');
    expect(response.statusCode).toBe(200);
    expect(response.text).toBe('Hello World!');
  });
});

describe('GET /date', () => {
  it('responds with a valid date', async () => {
    const response = await request(app).get('/date');
    expect(response.statusCode).toBe(200);
    expect(new Date(response.text).toString()).not.toBe('Invalid Date');
  });
  
});

describe('GET /info', () => {
  it('responds with system info', async () => {
    const response = await request(app).get('/info');
    expect(response.statusCode).toBe(200);
    expect(response.body).toHaveProperty('hostname');
    expect(response.body).toHaveProperty('platform');
    expect(response.body).toHaveProperty('arch');
    expect(response.body).toHaveProperty('userInfo');
    expect(response.body).toHaveProperty('cpuCores');
  });
  
});
