const request = require('supertest');
const { app, startServer } = require('../../app');
const logger = require('../../logger');

const server = startServer();

afterAll(() => {
  server.close();
});

describe('Custom logging middleware', () => {
  it('logs request information', async () => {
    const testUrl = '/hello';
    const testMethod = 'GET';
    const logSpy = jest.spyOn(logger, 'info');

    await request(app).get(testUrl);

    expect(logSpy).toHaveBeenCalled();

    const loggedData = logSpy.mock.calls.find((call) => {
      return call[0] === 'Request received' && call[1].url === testUrl && call[1].method === testMethod;
    });

    expect(loggedData).toBeTruthy();
    expect(loggedData[1]).toMatchObject({
      method: testMethod,
      url: testUrl,
    });

    logSpy.mockRestore();
  });
});